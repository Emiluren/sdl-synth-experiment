use sdl2::audio::{AudioCallback, AudioSpecDesired};
use std::time::Duration;

struct SquareWave {
    phase_inc: f32,
    phase: f32,
    volume: f32,
}

impl AudioCallback for SquareWave {
    type Channel = f32;

    fn callback(&mut self, out: &mut [f32]) {
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                self.volume
            } else {
                -self.volume
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();

    let desired_spec = AudioSpecDesired {
        freq: Some(44100),
        channels: Some(1),
        samples: None, // default sample size
    };

    let mut freq = 44100;
    let mut device = audio_subsystem.open_playback(None, &desired_spec, |spec| {
        freq = spec.freq;
        SquareWave {
            phase_inc: 440.0 / spec.freq as f32,
            phase: 0.0,
            volume: 0.1,
        }
    }).unwrap();

    device.resume();
    std::thread::sleep(Duration::from_millis(500));

    // Change the frequency an octave up
    {
        let mut callback = device.lock();
        *callback = SquareWave {
            phase_inc: 880.0 / freq as f32,
            phase: callback.phase * 2.,
            volume: 0.1,
        };
    }
    std::thread::sleep(Duration::from_millis(500));
}
